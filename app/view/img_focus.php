<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Get name from php</title>
		<link rel="stylesheet" href="../../ressource/style/img_focus.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<?php readfile("./header.php"); ?>
	<div class="imgfocus-separator"></div>
	<div id="img_viewer">
		<div id="like_but" class="cap-icon fa fa-heart-o"></div>
	</div>
	<div class="imgfocus-separator"></div>
	<section>Information section</section>
	<section>Comment section</section>
		<?php readfile("./footer.php"); ?>
	</body>
	<script type="text/javascript" src="../../ressource/script/script.js"></script>
</html>
