<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>Gallery</title>
		<link rel="stylesheet" href="../../ressource/style/gallery.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<meta name="viewport" content="width=device-width, initial-scale=1">
	</head>
	<body>
		<?php readfile("./header.php"); ?>
	<div class="gallery-separator"></div>
	<div id="gallery-container">
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
		<?php readfile("./capsule.php"); ?>
	</div>

	<div id="end-container">
		<i class="gal-icon fa fa-chevron-left"></i>
		<span class="gal-num">0</span>
		<i class="gal-icon fa fa-chevron-right"></i>
	</div>
		<?php readfile("./footer.php"); ?>

	</body>
	<script type="text/javascript" src="../../ressource/script/script.js"></script>
</html>
