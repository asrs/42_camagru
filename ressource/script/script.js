//============================================================================\\
//----------------------------------------------------------------------------\\
// DECLARATION GLOBAL
const puts = console.log

//============================================================================\\
//----------------------------------------------------------------------------\\
// FUNCTION

const postAjax = (url, data, success) => {
    var params = typeof data == 'string' ? data : Object.keys(data).map(
            (k) => { return encodeURIComponent(k) + '=' + encodeURIComponent(data[k]) }
        ).join('&');
    var xhr = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    xhr.open('POST', url);
    xhr.onreadystatechange = () => {
        if (xhr.readyState>3 && xhr.status==200) { success(xhr.responseText); }
    };
    xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    xhr.send(params);
    return xhr;
}

const removeElementsByClass = (className) => {
	var elements = document.getElementsByClassName(className);
	while (elements.length > 0) {
		elements[0].parentNode.removeChild(elements[0]);
	}
};

//----------------------------------------------------------------------------\\

const killPopup= () => {
	removeElementsByClass("back-container");
	removeElementsByClass("register-container");
	removeElementsByClass("sign-container");
};

//----------------------------------------------------------------------------\\

const callRegister = () => {
	var Element = document.getElementsByClassName("back-container");
	Element.onclick = "killRegister()";
	removeElementsByClass("sign-container");

	var html_ob = `<div class="register-container">
	<div class="c_head"></div>
	<div class="c_body">
		<form action="controller-2.php" method="post">
			<hr/>
			<input class="c_input" type="text" name="login" placeholder="enter your username" required>
			<hr/>
			<input class="c_input" type="text" name="email" placeholder="Enter your email" required>
			<hr/>
			<input class="c_input" type="password" name="psswd" placeholder="enter your password" required>
			<hr/>
			<input class="c_input" type="password" name="cfm_psswd" placeholder="confirm your password" required>
			<hr/>
			<input class="c_submit" type="submit" value="register">
		</form>
		<hr>
		<div class="c_help">
			<a onclick="killPopup();">cancel</a>
		</div>
	</div>
</div>`
	document.body.innerHTML = document.body.innerHTML + html_ob;
	bindHeader()
};

const callSignin = () => {
	var html_ob = `<div class="back-container" onclick="killPopup();"></div>
<div class="sign-container">
	<div class="c_head"><a href=""></a></div>
	<div class="c_body">
		<form action="controller-1.php" method="post">
			<hr/>
			<input class="c_input" type="text" name="login" placeholder="Username">
			<hr/>
			<input class="c_input" type="password" name="psswd" placeholder="password">
			<hr/>
			<input class="c_submit" type="submit" value="sign in">
		</form>
		<hr>
		<div class="c_help">
			<a href="" onclick="callRegister(); return false">register here</a>
			<a>forgot password ?</a>
		</div>
	</div>
</div>`

	document.body.innerHTML = document.body.innerHTML + html_ob;
	bindHeader()
};

//============================================================================\\
//----------------------------------------------------------------------------\\
// ACTION CALLER

const bindHeader = () => {
	var homeButton = document.getElementById("go_gallery");
	homeButton.addEventListener("click", () => {document.location.href = "../../app/view/gallery.php"});

	var studioButton = document.getElementById("go_studio");
	studioButton.addEventListener("click", () => {document.location.href = "../../app/view/studio.php"});

	var settingButton= document.getElementById("go_setting");
	settingButton.addEventListener("click", () => {document.location.href = "../../app/view/setting.php"});

	var signinButton= document.getElementById("go_signin");
	signinButton.addEventListener("click", () => callSignin());


//var signoutButton= document.getElementById("go_signout");
//homeButton.addEventListener("click", puts("cheh"));
};



//============================================================================\\
//----------------------------------------------------------------------------\\
// EXECUTER

bindHeader();
